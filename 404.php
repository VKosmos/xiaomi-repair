<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Xiaomi-repair
 */

get_header();
?>

	<section class="standard">
		<div class="container">
			<h1>Мы не смогли найти запрошенную страницу</h1>
		</div>
	</section>

<?php
get_footer();
