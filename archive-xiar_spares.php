<?php
/**
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Xiaomi-repair
 */

get_header();
?>

<section class="catalog">
	<div class="container">
		<h2 class="title">Запчасти для устройств</h2>
		<div class="catalog__flex">
			<button class="catalog__filter-btn"></button>

				<?php

					wp_nav_menu([
						'theme_location'			=> 'menu-catalog',
						'container'					=> 'nav',
						'container_class'			=> 'catalog__nav',
						'container_aria_label'	=> 'Каталог запчастей',
						'menu_class'				=> 'catalog__nav-list',
					]);

				?>

		</div>

		<ul class="catalog__list">
			<?php
				$i = 0;
				$attr_hide = '';
				if (have_posts()):
					while (have_posts()):
						the_post();
						$i++;
						if (9 === $i) {
							$attr_hide = ' data-hide-target="more"';
						} else if (10 === $i) {
							$attr_hide = '';
						}

						$img_id = get_post_thumbnail_id( get_the_ID() );
						$img_url = get_the_post_thumbnail_url( get_the_ID(), 'spares-catalog');
						$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
			?>

			<li class="catalog__item"<?php echo $attr_hide;?>>
				<div class="catalog__body">
					<a class="catalog__link" href="<?php the_permalink(get_the_ID());?>"></a>
					<img class="catalog__img" src="<?php echo $img_url; ?>" alt="<?php echo $alt; ?>">
					<h3 class="catalog__item-title"><?php the_title();?></h3>
				</div>
			</li>

			<?php
					endwhile;
				endif;
			?>
		</ul>

		<?php
			// $cnt = wp_count_posts('xiar_spares')->publish;
			if ($i > 8): ?>
		<div class="catalog__button-wrap">
			<button class="catalog__button" data-hide="more">Показать все запчасти</button>
		</div>
		<?php endif;?>

	</div>
</section>


<?php
get_footer();
