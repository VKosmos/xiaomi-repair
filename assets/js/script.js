jQuery(document).ready(function ($) {

	let reliableSlider = new Swiper('.reliable__slider', {
		pagination: {
		  el: '.reliable__pagination',
		  clickable: true,
		},

		navigation: {
		  nextEl: '.reliable__nav-button--next',
		  prevEl: '.reliable__nav-button--prev',
		},

	});

	let workprocessSlider = new Swiper('.work-process__slider', {
		autoHeight: true,
		pagination: {
			el: '.work-process__pagination',
			clickable: true,
			renderBullet: function (index, className) {
				let bulletStr = 'Контроль';
				switch (index) {
					case 0:
						bulletStr = 'Заявка';
						break;
					case 1:
						bulletStr = 'Диагностика';
						break;
					case 2:
						bulletStr = 'Согласование';
						break;
					case 3:
						bulletStr = 'Ремонт';
						break;
					case 4:
						bulletStr = 'Контроль';
						break;
				}

				return `
					<div class="${className}" data-index="${index + 1}">
						<span class="work-process__pagination-number">${index + 1}</span>
						<span class="work-process__pagination-text">${bulletStr}</span>
					</div>`;

			},
		},
    	navigation: {
	      nextEl: '.work-process__nav-button--next',
	      prevEl: '.work-process__nav-button--prev',
		},
	});


	/**
	 * Hide/Show prices (remont page)
	 */
	const $btnEl = $('[data-hide="price"]');
	const $hideEls = $('[data-hide-target="price"],[data-hide-target="price"] ~ div.price__row-wrapper');

	let flag = false;

	if ($btnEl && $hideEls) {

		$btnEl.on('click', () => {

			if (!flag) {
				$hideEls.slideDown();
				$btnEl.text('Свернуть все категории');
				$btnEl.addClass('price__show-all--active');
				flag = true;
			} else {
				$hideEls.slideUp();
				$btnEl.text('Смотреть все категории');
				$btnEl.removeClass('price__show-all--active');
				flag = false;
			}

		})
	}

	/**
	 * Hide/Show spares (taxonomy and archive pages)
	 */
	const $btnSparesEl = $('[data-hide="more"]');
	const $hideSparesEls = $('[data-hide-target="more"],[data-hide-target="more"] ~ li.catalog__item');

	let flagSpares = false;

	if ($btnSparesEl && $hideSparesEls) {

		$btnSparesEl.on('click', () => {

			if (!flagSpares) {
				$hideSparesEls.slideDown();
				$btnSparesEl.text('Скрыть запчасти');
				$btnSparesEl.addClass('catalog__button--active');
				flagSpares = true;
			} else {
				$hideSparesEls.slideUp();
				$btnSparesEl.text('Показать все запчасти');
				$btnSparesEl.removeClass('catalog__button--active');
				flagSpares = false;
			}

		})
	}


	/**
	 * Tabs on Statistics page
	 */
	const tabs = document.querySelector('.tabs-done');
	const tabsBtn = document.querySelectorAll('.tabs-done__btn');
	const tabsContent = document.querySelectorAll('.content-done__item');

	if (tabs) {
		tabs.addEventListener('click', (e) => {
			if (e.target.classList.contains('tabs-done__btn')) {
				const tabsPath = e.target.dataset.tabsPath;
				tabsBtn.forEach(el => {el.classList.remove('tabs-done__btn--active')});
				document.querySelector(`[data-tabs-path="${tabsPath}"]`).classList.add('tabs-done__btn--active');
				tabsHandler(tabsPath);
			}
		});
	}

	const tabsHandler = (path) => {
		tabsContent.forEach(el => {el.classList.remove('content-done__item--active')});
		document.querySelector(`[data-tabs-target="${path}"]`).classList.add('content-done__item--active');
	};


	/**
	 * Yandex Map on Contacts page
	 */
	if ($('body').hasClass('page-contacts')) {

		// console.log(WPJS);

		// console.log('contacts page');

		ymaps.ready(init);

		function init() {
			let myMap = new ymaps.Map("map", {
				// Координаты центра карты
				center: [WPJS.latitude, WPJS.longitude],
				// Масштаб карты
				zoom: 16,
				// Выключаем все управление картой
				controls: []
			});

			myMap.geoObjects
			.add(new ymaps.Placemark([WPJS.latitude, WPJS.longitude], {
				 balloonContent: 'Ремонт <strong>Xiaomi</strong>'
			}, {
				 preset: 'islands#icon',
				 iconColor: '#ff0000'
			}))

		}
	}

	/**
	 * Scroll To prices on
	 */
	let $scrollBtnEl = $('._js-scroll');
	if ($scrollBtnEl) {
		$scrollBtnEl.click(function (ev) {
			ev.preventDefault();
			const target = $(this).attr('data-scroll');
			const $targetEl = $(target);

			if ($targetEl) {
				$('html, body').animate({
					scrollTop: $targetEl.offset().top
			  }, 1000);
			}

		});
	}


	/**
	 * Adding attributes to modal Service Order
	 */
	const $modalHiddenDevice = $('.appointment-form input[type="hidden"][name="device"]');
	const $modalHiddenService = $('.appointment-form input[type="hidden"][name="service-name"]');

	// debugger

	$('.price__order[data-vkpath]').on('click', function () {
		$modalHiddenDevice.attr('value', $(this).parents('.price__table[data-device]').attr('data-device'));
		$modalHiddenService.attr('value', $(this).attr('data-service'));
	})

});

