
</main>
	<footer class="footer">
		<div class="container">
			<div class="footer__flex-wrapper">
				<div class="footer__logo-wrap site-logo">

					<?php if (is_front_page()):?>
						<span class="site-logo__link">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/header/logo.png'?>" alt="" width="76" height="76">
						</span>
					<?php else: ?>
						<a class="site-logo__link" href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/header/logo.png'?>" alt="" width="76" height="76">
						</a>
					<?php endif; ?>

					<div class="site-logo__text-wrap">
						<span class="site-logo__name">XIAOMI</span>
						<span class="site-logo__description">Сервисный центр</span>
					</div>
				</div>
				<div class="footer__apply">
					<p class="footer__apply-text">По вопросам и предложениям</p>
					<a class="footer__email" href="mailto:<?= carbon_get_theme_option('xiam_email');?>"><?= carbon_get_theme_option('xiam_email');?></a>
				</div>
			</div>

			<div class="footer__wrapper">
				<h3 class="footer__title">Навигация</h3>

				<?php
					wp_nav_menu([
						'theme_location'			=> 'menu-footer-main',
						'container'					=> 'nav',
						'container_class'			=> 'footer__main-nav nav-footer',
						'container_aria_label'	=> 'Основное меню сайта',
						'menu_class'				=> 'nav-footer__list',
						'li_class'  				=> 'nav-footer__item'
					]);


					$address = carbon_get_theme_option('xiar_address_footer');
					if (!empty($address)):?>

						<h3 class="footer__title">Адрес</h3>
						<p class="footer__address"><?php echo $address;?></p>

					<?php endif; ?>



				<div class="footer__pay">
					<p class="footer__pay-text">Оплата картой:</p>
					<div class="footer__pay-wrapper">
						<img src="<?php echo get_template_directory_uri() . '/assets/images/footer/visa.png'?>" alt="">
						<img src="<?php echo get_template_directory_uri() . '/assets/images/footer/master_card.png'?>" alt="">
						<img src="<?php echo get_template_directory_uri() . '/assets/images/footer/mir.png'?>" alt="">
					</div>
				</div>

			</div>
		</div>
	</footer>

	<div class="modal">

		<div class="modal__table" data-vktarget="appointment">
			<div class="modal__table-cell">
				<div class="modal__container appointment-modal">
					<div class="modal__content appointment-modal__content">
						<button class="modal__close appointment-modal__close">Закрыть</button>

						<?php

							if (is_front_page()):

								echo '<h2 class="appointment-modal__title title">Оставьте заявку на <strong>ремонт</strong></h2>';
								echo do_shortcode( '[contact-form-7 id="89" title="Заказ ремонта" html_class="appointment-form"]' );

							elseif (is_singular( 'xiar_spares' )):

								echo '<h2 class="appointment-modal__title title">Оставьте заявку на <strong>запчасть</strong></h2>';
								echo do_shortcode( '[contact-form-7 id="94" title="Заказ запчасти" html_class="appointment-form" spare-name="' . get_the_title() . '"]' );

							elseif (is_singular( 'xiar_gadget' )):

								echo '<h2 class="appointment-modal__title title">Оставьте заявку на <strong>услугу</strong></h2>';
								echo do_shortcode( '[contact-form-7 id="98" title="Заказ услуги" html_class="appointment-form" device="" service-name="' . get_the_title() . '"]' );

							elseif (is_taxonomy( 'xiar_gadget_cat' )):
								$queried_object = get_queried_object();
								$title = $queried_object->name;

								echo '<h2 class="appointment-modal__title title">Оставьте заявку на <strong>услугу</strong></h2>';
								echo do_shortcode( '[contact-form-7 id="98" title="Заказ услуги" html_class="appointment-form" device="" service-name="' . $title . '"]' );

							endif;

						?>

					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="modal2"></div>

<?php wp_footer(); ?>

</body>
</html>
