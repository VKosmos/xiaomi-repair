<?php
/**
 * Xiaomi-repair functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Xiaomi-repair
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'xiar_setup' ) ) :

	function xiar_setup() {

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * All 4 menu
		 */
		require get_template_directory() . '/inc/menu.inc.php';


		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'xiar_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'xiar_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function xiar_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'xiar' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'xiar' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'xiar_widgets_init' );

/**
 * Enqueuing scripts and styles
 */
require_once get_template_directory() . '/inc/enqueue.inc.php';

/**
 * Customizer additions
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Carbon Fields
 */
require get_template_directory() . '/inc/carbon-fields/theme-options.inc.php';
require get_template_directory() . '/inc/carbon-fields/metaboxes.inc.php';


/**
 * Image sizes
 */
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'service-slide', 390, 540 );
	add_image_size( 'spares-thumb', 397, 400 );
	add_image_size( 'spares-catalog', 210, 210 );
	add_image_size( 'device-banner', 674, 674 );
}

/**
 * Registration of custom post types
 */
require get_template_directory() . '/inc/post-types.inc.php';

/**
 * Registration of custom taxonomies
 */
require get_template_directory() . '/inc/taxonomies.inc.php';

/**
 * Creating shortcode
 */
require get_template_directory() . '/inc/shortcodes.inc.php';

/**
 * Cron
 */
require get_template_directory() . '/inc/cron.inc.php';

add_filter( 'shortcode_atts_wpcf7', 'xiar_custom_shortcode_atts_wpcf7', 10, 3 );
function xiar_custom_shortcode_atts_wpcf7( $out, $pairs, $atts ) {

	if( isset($atts['spare-name']) ) {
		$out['spare-name'] = $atts['spare-name'];
	}

	if( isset($atts['device']) ) {
		$out['device'] = $atts['device'];
	}

	if( isset($atts['service-name']) ) {
		$out['service-name'] = $atts['service-name'];
	}

	return $out;
}