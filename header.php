<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php (isset($args[0])) ? body_class($args[0]) : body_class(); ?>>
<?php wp_body_open(); ?>

	<header class="header">
		<div class="header-top">
			<div class="header-top__flex container">
				<a class="header-top__menu" href="#" data-vkpath="modal-nav" data-vkcontainer="2">
					<span></span>
				</a>

				<nav class="modal__content modal2__wrapper nav-modal" data-vktarget="modal-nav">
				<?php
					wp_nav_menu([
						'theme_location'			=> 'menu-header-main-mobile',
						'container'					=> false,
						'menu_class'				=> 'nav-modal__list',
						'li_class'  				=> 'nav-modal__item'
					]);
				?>
				</nav>

				<div class="header-top__address">
				</div>
				<div class="header-top__time">
					<p>Будни <?php echo carbon_get_theme_option('xiar_work_weekdays');?></p>
					<p>Выходные <?php echo carbon_get_theme_option('xiar_work_weekends');?></p>
				</div>
				<div class="header-top__phone" data-da=".header__flex,1023,1">
					<?php
						$phone = carbon_get_theme_option('xiam_phone');
						if ($phone):
							$phone_link = preg_replace("/[^+0-9]/", '', $phone);?>

							<a href="tel:<?=$phone_link;?>">
								<img src="<?php echo get_template_directory_uri() . '/assets/images/header/phone.svg'?>" alt=""><?= $phone;?></a>
						<?php
						endif; ?>
				</div>
			</div>
		</div>
		<div class="header-middle">
			<div class="container">
				<div class="header__flex">
					<div class="header__logo-wrap site-logo">

						<?php if (is_front_page()):?>
							<span class="site-logo__link">
								<img src="<?php echo get_template_directory_uri() . '/assets/images/header/logo.png'?>" alt="" width="76" height="76">
							</span>
						<?php else: ?>
							<a class="site-logo__link" href="<?php echo home_url(); ?>">
								<img src="<?php echo get_template_directory_uri() . '/assets/images/header/logo.png'?>" alt="" width="76" height="76">
							</a>
						<?php endif; ?>

						<div class="site-logo__text-wrap">
							<span class="site-logo__name">XIAOMI</span>
							<span class="site-logo__description">Сервисный центр</span>
						</div>
					</div>

					<?php
					wp_nav_menu([
						'theme_location'			=> 'menu-header-main',
						'container'					=> 'nav',
						'container_class'			=> 'header__main-nav nav-header',
						'container_aria_label'	=> 'Основное меню сайта',
						'menu_class'				=> 'nav-header__list',
						'li_class'  				=> 'nav-header__item'
					]);
					?>

				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<nav class="header__catalog-nav nav-catalog">

					<?php
					wp_nav_menu([
						'theme_location'			=> 'menu-header-products',
						'container'					=> false,
						'menu_class'				=> 'nav-catalog__list',
						'li_class'  				=> 'nav-catalog__item'
					]);
					?>

					<div class="modal__content modal2__wrapper categories-modal" data-vktarget="modal-categories">
						<?php
						wp_nav_menu([
							'theme_location'			=> 'menu-header-other-products',
							'container'					=> false,
							'menu_class'				=> 'categories-modal__list',
							'li_class'  				=> 'categories-modal__item'
						]);
						?>
					</div>

				</nav>

			</div>
		</div>
	</header>

	<main>