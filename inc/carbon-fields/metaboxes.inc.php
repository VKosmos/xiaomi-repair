<?php
	if (!defined('ABSPATH')) return;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_metaboxes' );
function crb_attach_metaboxes() {

	Container::make( 'post_meta', 'FAQ', 'Настройки' )
		->where( 'post_type', '=', 'page' )
		->where( 'post_template', '=', 'templates/template-main.php' )
		->add_tab('Баннер', [
			Field::make( 'text', 'xiar_main_banner_title', 'Залоговок'),
			Field::make( 'text', 'xiar_main_banner_text', 'Текст'),
		])
		->add_tab('Слайдер Наш сервисный центр', [
			Field::make( 'text', 'xiar_main_service_title', 'Заголовок' ),
			Field::make( 'text', 'xiar_main_service_text', 'Описание' ),
			Field::make( 'complex', 'xiam_service_center', 'Слайды' )
				->add_fields( [
					Field::make( 'image', 'xiam_service_slide_image', 'Изображение слайда' )
						->set_width(30),
					Field::make( 'text', 'xiam_service_slide_title', 'Заголовок слайда' )
						->set_width(35),
					Field::make( 'textarea', 'xiam_service_slide_text', 'Текст слайда' )
						->set_width(35),
				])
				->set_layout('tabbed-vertical')
		])
		->add_tab('Ремонт честно', [
			Field::make( 'text', 'xiar_main_fair_title', 'Залоговок'),
			Field::make( 'textarea', 'xiar_main_fair_subtitle', 'Подзалоговок')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_fair_text1', 'Первый текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_fair_text2', 'Второй текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_fair_text3', 'Третий текст')
				->set_width(50),
		])
		->add_tab('Типичные поломки', [
			Field::make( 'text', 'xiar_main_typical_title', 'Залоговок'),
			Field::make( 'textarea', 'xiar_main_typical_text1', 'Первый текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text2', 'Второй текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text3', 'Третий текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text4', 'Четвёртый текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text5', 'Пятый текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text6', 'Шестой текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_typical_text7', 'Седьмой текст')
				->set_width(50),
		])
		->add_tab('Качественный ремонт', [
			Field::make( 'text', 'xiar_main_quality_title', 'Залоговок'),
			Field::make( 'textarea', 'xiar_main_quality_text1', 'Первый текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_quality_text2', 'Второй текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_quality_text3', 'Третий текст')
				->set_width(50),
			Field::make( 'textarea', 'xiar_main_quality_text4', 'Четвёртый текст')
				->set_width(50),
		])
		->add_tab('Слайдер Процесс работы', [
			Field::make( 'complex', 'xiam_work_process', 'Слайды' )
				->add_fields( [
					Field::make( 'image', 'xiam_work_slide_image', 'Изображение слайда' )
						->set_width(30),
					Field::make( 'text', 'xiam_work_slide_title', 'Заголовок слайда' )
						->set_width(35),
					Field::make( 'textarea', 'xiam_work_slide_text', 'Текст слайда' )
						->set_width(35),
				])
				->set_layout('tabbed-vertical')
		]);

	/**
	 * Gadgets - single
	 */
	Container::make( 'post_meta', 'Divece Settings', 'Настройки' )
		->where( 'post_type', '=', 'xiar_gadget' )
		->add_tab('Баннер', [
			Field::make( 'text', 'xiar_gadget_banner_title', 'Заголовок'),
			Field::make( 'textarea', 'xiar_gadget_banner_text', 'Текст'),
			Field::make( 'image', 'xiar_gadget_banner_image', 'Фон')
				->set_width(50),
			Field::make( 'image', 'xiar_gadget_banner_image_mobile', 'Фон для мобильных')
				->set_width(50),
		])
		->add_tab('Текстовые блоки', [
			Field::make( 'text', 'xiar_gadget_price-title', 'Заголок прайса' ),
		])
		->add_tab('Список услуг', [
			Field::make( 'complex', 'xiar_repair_cat', 'Услуги' )
				->add_fields( [
					Field::make( 'text', 'xiar_gadget_repair_cat_name', 'Название группы услуг' ),
					Field::make( 'complex', 'xiar_gadget_repair_services_list', 'Список услуг' )
						->add_fields([
							Field::make( 'text', 'xiar_gadget_repair_service', 'Название услуги' )
								->set_width(34),
							Field::make( 'text', 'xiar_gadget_repair_service_price', 'Стоимость услуги' )
								->set_width(33),
							Field::make( 'text', 'xiar_gadget_repair_service_time', 'Время' )
								->set_width(33),
						])
						->setup_labels([
							'plural_name'		=> 'Услугу',
							'singular_name'	=> 'Услуги',
						])
				])
				->set_layout('tabbed-vertical')
				->setup_labels([
					'plural_name'		=> 'Группы',
					'singular_name'	=> 'Группу',
				])
		]);


	/**
	 * Gadgets - taxonomy
	 */
	Container::make( 'term_meta', 'Device categor Settings', 'Настройки' )
		->where( 'term_taxonomy', '=', 'xiar_gadget_cat' )
		->add_tab('Баннер', [
			Field::make( 'text', 'xiar_gadgetcat_banner_title', 'Заголовок'),
			Field::make( 'textarea', 'xiar_gadgetcat_banner_text', 'Текст'),
			Field::make( 'image', 'xiar_gadgetcat_banner_image', 'Фон')
				->set_width(50),
			Field::make( 'image', 'xiar_gadgetcat_banner_image_mobile', 'Фон для мобильных')
				->set_width(50),
		])
		->add_tab('Текстовые блоки', [
			Field::make( 'text', 'xiar_gadgetcat_price-title', 'Заголок прайса' ),
		])
		->add_tab('Список услуг', [
			Field::make( 'complex', 'xiar_gadgetcat_repair_cat', 'Услуги' )
				->add_fields( [
					Field::make( 'text', 'xiar_gadgetcat_repair_cat_name', 'Название группы услуг' ),
					Field::make( 'complex', 'xiar_gadgetcat_repair_services_list', 'Список услуг' )
						->add_fields([
							Field::make( 'text', 'xiar_gadgetcat_repair_service', 'Название услуги' )
								->set_width(34),
							Field::make( 'text', 'xiar_gadgetcat_repair_service_price', 'Стоимость услуги' )
								->set_width(33),
							Field::make( 'text', 'xiar_gadgetcat_repair_service_time', 'Время' )
								->set_width(33),
						])
						->setup_labels([
							'plural_name'		=> 'Услугу',
							'singular_name'	=> 'Услуги',
						])
				])
				->set_layout('tabbed-horizontal')
				->setup_labels([
					'plural_name'		=> 'Группы',
					'singular_name'	=> 'Группу',
				])
		]);

	/**
	 * Запчасти
	 */
	Container::make( 'post_meta', 'Our services', 'Настройки' )
		->where( 'post_type', '=', 'xiar_spares' )
		->add_fields([
			Field::make( 'text', 'xiar_spares_price', 'Цена' ),
		]);
};

