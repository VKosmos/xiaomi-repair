<?php
	if (!defined('ABSPATH')) return;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {
	Container::make( 'theme_options', 'Theme options' )
		->set_page_menu_title( 'Настройки темы' )
		->set_icon( 'dashicons-admin-generic')
		->add_tab( 'Контакты', [
			Field::make( 'text', 'xiam_email', 'E-mail' ),
			Field::make( 'text', 'xiam_phone', 'Контактный телефон' ),
			Field::make( 'textarea', 'xiam_address', 'Адрес' ),
			Field::make( 'text', 'xiar_address_footer', 'Адрес для подвала сайта' ),
			Field::make( 'text', 'xiam_latitude', 'Широта' )
				->set_width(50),
			Field::make( 'text', 'xiam_longitude', 'Долгота' )
				->set_width(50),
			Field::make( 'text', 'xiar_work_weekdays', 'Режим работы в будни' )
				->set_width(50),
			Field::make( 'text', 'xiar_work_weekends', 'Режим работы в выходные' )
				->set_width(50),
		  ])
		->add_tab( 'Статистика', [
			Field::make( 'text', 'xiar_stat_grow', 'Ежедневный прирост')
				->set_default_value(10),
			Field::make( 'complex', 'xiar_stat_tabs', 'Вкладки')
				->add_fields([
					Field::make( 'text', 'xiar_stat_name', 'Наименование')
						->set_width(50),
					Field::make( 'image', 'xiar_stat_tab-img', 'Изображение устройств')
						->set_width(50),
					Field::make( 'complex', 'xiar_stat_data', 'Устройства')
						->add_fields([
							Field::make( 'text', 'xiar_stat_device', 'Название')
								->set_width(50)
								->set_required(true),
							Field::make( 'text', 'xiar_stat_cnt', 'Количество')
								->set_width(50)
								->set_required(true)
								->set_attribute('placeholder', 'Количество отремонтированных устройств'),
						])
						->setup_labels([
							'plural_name'		=> 'Устройства',
							'singular_name'	=> 'Устройство',
						])
						->set_max(4)
				])
				->set_layout('tabbed-horizontal')
				->setup_labels([
					'plural_name'		=> 'Вкладки',
					'singular_name'	=> 'Вкладку',
				])
				->set_max(3)
		]);

};
