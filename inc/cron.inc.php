<?php
	if (!defined('ABSPATH')) return;


add_action('wp', 'xiar_activate_cron');
function xiar_activate_cron()
{

	if( !wp_next_scheduled( 'xiar_grow_repaired' ) ) {
		wp_schedule_event( time(), 'daily', 'xiar_grow_repaired');
	}

}

add_action('xiar_grow_repaired', 'xiar_grow_repaired_data');
function xiar_grow_repaired_data()
{

	$grow = (int)carbon_get_theme_option('xiar_stat_grow');
	$grow = (!empty($grow)) ? $grow : 1;
	$data = carbon_get_theme_option('xiar_stat_tabs');
	if (!empty($data)) {
		foreach($data as &$d) {
			foreach ($d['xiar_stat_data'] as &$v) {
				$temp = (int)$v['xiar_stat_cnt'];
				$temp += $grow;
				$v['xiar_stat_cnt'] = $temp;
			}
		}
	}
	carbon_set_theme_option('xiar_stat_tabs', $data);

}

