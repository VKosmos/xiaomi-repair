<?php

if (!defined('ABSPATH')) return;


function xiar_styles() {
	wp_enqueue_style( 'xiar-style', get_stylesheet_uri(), array(), _S_VERSION );

	wp_enqueue_style( 'xiar-main', get_template_directory_uri() . '/assets/css/style.css', array(), rand(1,999));
	wp_enqueue_style( 'swiper-slider', get_template_directory_uri() . '/assets/css/swiper-bundle.min.css', array(), rand(1,999));

}
add_action( 'wp_enqueue_scripts', 'xiar_styles' );


function xiar_scripts() {
	wp_enqueue_script( 'xiar-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'xiar-modal', get_template_directory_uri() . '/assets/js/modal.js', array(), rand(1,999), true );
	wp_enqueue_script( 'xiar-dynamic-adapt', get_template_directory_uri() . '/assets/js/dynamicAdapt.js', array(), rand(1,999), true );
	wp_enqueue_script( 'swiper-slider', get_template_directory_uri() . '/assets/js/swiper-bundle.min.js', array(), rand(1,999), true );

	if (is_page('kontakty')) {
		wp_enqueue_script ('xiar-ymap', 'http://api-maps.yandex.ru/2.1/?lang=ru_RU', null, null, true);

		wp_enqueue_script( 'xiar-main', get_template_directory_uri() . '/assets/js/script.js', array('jquery', 'swiper-slider', 'xiar-ymap'), rand(1,999), true );
		// wp_localize_script( 'xiar-main', 'WPJS', ['latitude' => 11, 'longitude' => 22] );
		wp_localize_script( 'xiar-main', 'WPJS', ['latitude' => carbon_get_theme_option('xiam_latitude'), 'longitude' => carbon_get_theme_option('xiam_longitude')] );
	} else {
		wp_enqueue_script( 'xiar-main', get_template_directory_uri() . '/assets/js/script.js', array('jquery', 'swiper-slider'), rand(1,999), true );
	}

}
add_action( 'wp_enqueue_scripts', 'xiar_scripts' );

