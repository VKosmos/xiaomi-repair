<?php
	if (!defined('ABSPATH')) return;

// This theme uses wp_nav_menu() in one location.
register_nav_menus(
	array(
		'menu-header-main' => 'Основное в шапке',
		'menu-header-main-mobile' => 'Основное для мобильных устройств',
		'menu-header-products' => 'Устройства в шапке',
		'menu-header-other-products' => 'Устройства Другие в шапке',
		'menu-footer-main' => 'Основное в футере',
		'menu-catalog' => 'Рубрики каталога запчастей',
	)
);

// Реализация добавления кастомных классов в ссылки меню
add_filter( 'nav_menu_css_class', 'xiar_custom_nav_class', 10, 3 );
function xiar_custom_nav_class($classes, $item, $args){

	if ('menu-header-main' === $args->theme_location) {
		if ('korzina' === $item->post_name) {
			$classes[] = 'nav-header__item--cart';
		}
	}

	if ('menu-header-main-mobile' === $args->theme_location) {
		if ('korzina' === $item->post_name) {
			$classes[] = 'nav-modal__item--cart';
		}
	}

	if (isset($args->li_class)){
		$classes[] = $args->li_class;
	}

	return $classes;
}

// Добавляем data атрибуты для работы модальных окон
add_filter( 'nav_menu_link_attributes', 'xiar_add_modal_attributes_to_link', 10, 3 );
function xiar_add_modal_attributes_to_link( $atts, $item, $args ) {

	if ('menu-header-products' === $args->theme_location) {
		if (in_array('nav-catalog__item--others', $item->classes)) {
			$atts['data-vkpath'] = 'modal-categories';
			$atts['data-vkcontainer'] = '2';
		}
	}

	return $atts;
}