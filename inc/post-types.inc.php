<?php
	if (!defined('ABSPATH')) return;

add_action( 'init', 'kedrm_register_post_types' );
function kedrm_register_post_types(){
	// register_post_type( 'xiar_devices', [
	// 	'label'  => null,
	// 	'labels' => [
	// 		'name'               => 'Типы устройств',
	// 		'singular_name'      => 'Тип устройств',
	// 		'add_new'            => 'Добавить новый', // для добавления новой записи
	// 		'add_new_item'       => 'Добавление нового', // заголовка у вновь создаваемой записи в админ-панели.
	// 		'edit_item'          => 'Редактирование типа устройства', // для редактирования типа записи
	// 		'new_item'           => 'Новый тип устройства', // текст новой записи
	// 		'view_item'          => 'Смотреть тип устройства', // для просмотра записи этого типа.
	// 		'search_items'       => 'Искать тип устройства', // для поиска по этим типам записи
	// 		'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
	// 		'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
	// 		'parent_item_colon'  => '', // для родителей (у древовидных типов)
	// 		'menu_name'          => 'Типы устройств', // название меню
	// 	],
	// 	'description'         => '',
	// 	'public'              => true,
	// 	'show_in_menu'        => true, // показывать ли в меню адмнки
	// 	'show_in_rest'        => false, // добавить в REST API. C WP 4.7
	// 	'rest_base'           => null, // $post_type. C WP 4.7
	// 	'menu_position'       => null,
	// 	'menu_icon'           => 'dashicons-games',
	// 	'capability_type'   	 => 'post',
	// 	//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
	// 	//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
	// 	'hierarchical'        => false,
	// 	'supports'            => [ 'title', 'thumbnail', ],
	// 	'taxonomies'          => [],
	// 	'has_archive'         => false,
	// 	'rewrite'             => true,
	// 	'query_var'           => true,
	// 	'rewrite'					=> [
	// 		'slug'	=> 'devices',
	// 	]
	// ] );


	register_post_type( 'xiar_gadget', [
		'label'  => null,
		'labels' => [
			'name'               => 'Устройства',
			'singular_name'      => 'Устройство',
			'add_new'            => 'Добавить новое', // для добавления новой записи
			'add_new_item'       => 'Добавление нового', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование устройства', // для редактирования типа записи
			'new_item'           => 'Новое устройство', // текст новой записи
			'view_item'          => 'Смотреть устройство', // для просмотра записи этого типа.
			'search_items'       => 'Искать устройство', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Устройства', // название меню
		],
		'description'         => '',
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_rest'        => false, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-games',
		'capability_type'   	 => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'thumbnail', ],
		'taxonomies'          => [ 'xiar_gadget_cat' ],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
		'rewrite'					=> [
			'slug'	=> 'gadgets',
		]
	] );


	register_post_type( 'xiar_spares', [
		'label'  => null,
		'labels' => [
			'name'               => 'Запчасти',
			'singular_name'      => 'Запчасть',
			'add_new'            => 'Добавить новую', // для добавления новой записи
			'add_new_item'       => 'Добавление новой', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование запчасти', // для редактирования типа записи
			'new_item'           => 'Новая запчасть', // текст новой записи
			'view_item'          => 'Смотреть запчасть', // для просмотра записи этого типа.
			'search_items'       => 'Искать запчасть', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Запчасти', // название меню
		],
		'description'         => '',
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		// 'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-tools',
		'capability_type'   	 => 'post',
		// 'capabilities'      	=> 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'thumbnail', 'editor' ],
		'taxonomies'          => [ 'xiar_spares_cat' ],
		'has_archive'         => true,
		'rewrite'             => true,
		'query_var'           => true,
		'rewrite'					=> [
			'slug'	=> 'spares',
		]
	] );


};
