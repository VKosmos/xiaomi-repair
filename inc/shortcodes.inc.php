<?php
	if (!defined('ABSPATH')) return;

	add_shortcode( 'statistics-repaired', 'xiar_shortcode_statistics_repaired' );
	function xiar_shortcode_statistics_repaired() {

		$data = carbon_get_theme_option('xiar_stat_tabs');
		if (!empty($data)):

			ob_start();
		?>

		<section class="done">
			<div class="container">
				<h2 class="title">Мы уже сделали</h2>
				<div class="done__tabs tabs-done">

					<div class="tabs-done__buttons">
						<?php
							$i = 1;
							$active_class= " tabs-done__btn--active";
							foreach ($data as $v):
						?>
							<button class="tabs-done__btn<?php echo $active_class;?>" data-tabs-path="tab<?php echo $i;?>"><?php echo $v['xiar_stat_name'];?></button>
						<?php
							$active_class = '';
							$i++;
							endforeach;
						?>
					</div>

					<div class="tabs-done__content content-done">
						<?php
							$i = 1;
							$active_class= " content-done__item--active";
							foreach ($data as $v):

								$img_id = $v['xiar_stat_tab-img'];
								$img_url = wp_get_attachment_image_url( $img_id, 'full' );
								$img_alt = (!empty($img_id)) ? get_post_meta($img_id, '_wp_attachment_image_alt', true) : '';

						?>
							<div class="content-done__item<?php echo $active_class;?>" data-tabs-target="tab<?php echo $i;?>">

								<img class="content-done__img-product" src="<?php echo $img_url;?>" alt="<?php echo $img_alt;?>">

								<div class="content-done__stat">
									<div class="content-done__data data-done">

										<?php
											foreach ($v['xiar_stat_data'] as $p):
												$height_percentage = $p['xiar_stat_cnt'] / 10000;
												if ($height_percentage < 0) $height_percentage = 0;
												if ($height_percentage > 1) $height_percentage = 1;
												$height_percentage = $height_percentage * 100;

										?>
										<div class="data-done__item" style="height: <?php echo $height_percentage;?>%">
											<p><?php echo $p['xiar_stat_device'];?></p>
										</div>
										<?php
											endforeach;
										?>

									</div>
									<div class="content-done__graph"><span>10000</span><span>9000</span><span>8000</span><span>7000</span><span>6000</span><span>5000</span><span>4000</span><span>3000</span><span>2000</span><span>1000</span></div>

								</div>
							</div>
						<?php
							$active_class = '';
							$i++;
							endforeach;
						?>

					</div>

				</div>
			</div>
      </section>

		<?php
			$html = ob_get_clean();
			return $html;

		endif;

	}
