<?php
	if (!defined('ABSPATH')) return;

add_action( 'init', 'xiar_register_taxonomies' );
function xiar_register_taxonomies(){

	$args = array(
		'labels' => array(
			'name'					=> 'Раздел',
			'menu_name' 			=> 'Разделы',
			'search_items'			=> 'Искать разделы',
			'all_items'				=> 'Все разделы',
			'view_item'				=> 'Открыть раздел',
			// 'parent_item'			=> 'Parent Genre',
			// 'parent_item_colon'	=> 'Parent Genre:',
			'edit_item'				=> 'Редактировать раздел',
			'update_item'			=> 'Обновить раздел',
			'add_new_item'			=> 'Добавить новый раздел',
			'new_item_name'		=> 'Новый раздел',
		),
		'public' 				=> true,
		'show_ui' 				=> true,
		'hierarchical'			=> true,
		'show_admin_column'	=> true,
		'show_in_rest'			=> true,
		'show_in_quick_edit'	=> true,
		'rewrite'					=> [
			'slug'	=> 'cat',
		]
	);
	register_taxonomy( 'xiar_spares_cat', 'xiar_spares', $args );


	$args = array(
		'labels' => array(
			'name'					=> 'Категория',
			'menu_name' 			=> 'Категории',
			'search_items'			=> 'Искать категории',
			'all_items'				=> 'Все категории',
			'view_item'				=> 'Открыть категорию',
			// 'parent_item'			=> 'Parent Genre',
			// 'parent_item_colon'	=> 'Parent Genre:',
			'edit_item'				=> 'Редактировать категорию',
			'update_item'			=> 'Обновить категорию',
			'add_new_item'			=> 'Добавить новую категорию',
			'new_item_name'		=> 'Новая категория',
		),
		'public' 				=> true,
		'show_ui' 				=> true,
		'hierarchical'			=> true,
		'show_admin_column'	=> true,
		'show_in_rest'			=> true,
		'show_in_quick_edit'	=> true,
		'rewrite'					=> [
			'slug'	=> 'gadgets_category',
		]
	);
	register_taxonomy( 'xiar_gadget_cat', 'xiar_gadget', $args );




}
