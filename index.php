<?php
/**
 * The Main page template
 *
 * @package Xiaomi-repair
 */

get_header(null, ['page-main']);
?>

	<section class="main-screen">
		<div class="main-screen__flex">
			<div class="main-screen__text-wrap">
				<h1 class="main-screen__title">Сервисный центр <span>Xiaomi</span><br>в Москве</h1>
				<p class="main-screen__text">Профессиональный ремонт техники Xiaomi</p>
				<a class="main-screen__button" href="#" data-vkpath="appointment" data-vkcontainer="1">Записаться на ремонт</a>
			</div>
		</div>
	</section>

	<section class="reliable">
		<div class="container">
			<h2 class="title">Ищите <span>надёжный</span> сервисный центр?</h2>
			<p class="reliable__headline">Мы - команда профессионалов с современными технологиями<br>и наша миссия -
				вернуть ваше устройство к жизни!</p>

			<?php
				$slides = carbon_get_post_meta(get_the_ID(), 'xiam_service_center');
				if ($slides):

			?>
			<div class="reliable__slider-wrapper">
				<div class="reliable__slider swiper-container">
					<ul class="reliable__list swiper-wrapper">

						<?php	foreach ($slides as $v):
							$img_id = $v['xiam_service_slide_image'];
							$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
						?>

						<li class="reliable__item swiper-slide">
							<div class="reliable__image-wrapper">
								<img src="<?php echo wp_get_attachment_image_url($img_id, 'service-slide'); ?>" alt="<?=$alt;?>">
							</div>
							<div class="reliable__text-wrapper">
								<h3 class="reliable__head"><?php echo $v['xiam_service_slide_title'];?></h3>
								<div class="reliable__text">
									<?php echo $v['xiam_service_slide_text'];?>
								</div>
							</div>
						</li>

						<?php endforeach;?>

					</ul>
				</div>
				<div class="reliable__pagination"></div>
				<button class="reliable__nav-button reliable__nav-button--prev"></button>
				<button class="reliable__nav-button reliable__nav-button--next"></button>
			</div>
			<?php endif;?>

			<?php the_content(); ?>

		</div>
	</section>



	<section class="fair">
		<div class="container">
			<h2 class="title title--left">Ремонт <strong>Xiaomi</strong> честно</h2>
			<p class="fair__headline"><strong>Xiaomi</strong> — это один из крупнейших производителей в мире.<br>Но
				никакая техника не может быть идеальной, поломки бывают неизбежны.</p>
			<div class="fair__wrapper">
				<div class="fair__text-wrapper">
					<p>Мы много лет занимаемся ремонтом техники Xiaomi и можем отремонтировать смартфон, планшет, ноутбук,
						робот-пылесос, электросамокат и квадрокоптер. Общие проблемы Xiaomi, такие как проблемы с
						программным обеспечением или некоторые недостатки дизайна, для нас не новы. Мы берем на себя все
						ремонтные работы Xiaomi, начиная с разбитого экрана, проблем с портом зарядки, замены аккумулятора,
						объектива камеры или заднего стекла.</p>
				</div>
				<div class="fair__text-wrapper fair__text-wrapper--two">
					<p>Большинство запасных частей Xiaomi мы храним на складе. Если запасная часть недоступна в России, мы
						можем заказать ее напрямую у китайского поставщика, если только вы можете подождать еще несколько
						дней.</p>
				</div>
				<div class="fair__text-wrapper fair__text-wrapper--three">
					<p>Выберите услугу ремонта Xiaomi, которая вам нужна. Затем просто закажите ремонт. Мы заботимся о
						вашей технике и после ремонта. Гарантия на большинство ремонтных услуг составляет от 3-х месяцев до
						1-го года.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="breaking">
		<div class="container">
			<h2 class="title">Типичные поломки смартфонов <strong>Xiaomi</strong></h2>
			<ul class="breaking__list">
				<li class="breaking__item breaking__item--diagnostics">Каждый ремонт мы начинаем с диагностики для
					локализации источника неисправностей.</li>
				<li class="breaking__item breaking__item--soldering breaking__item--right">Ремонт микропайки: Ремонт
					уровня платы с помощью микроскопа. Мы решаем проблемы, которые не могут исправить другие. У нас есть
					самое лучшее оборудование для такого ремонта.</li>
				<li class="breaking__item breaking__item--screen">Ремонт сломанного экрана: Разбитое переднее стекло,
					проблемы с тачскрином, вертикальные линии на экране или ничего на экране? Мы можем починить это в
					течение дня.</li>
				<li class="breaking__item breaking__item--charge breaking__item--right">Ремонт порта зарядки: Мы
					диагностируем проблему с портом зарядки и заменяем его, если он неисправен. Ремонт обычно занимает 1
					час.</li>
				<li class="breaking__item breaking__item--camera">Исправление камеры и объектив: Сломанный объектив
					камеры или проблемы с автофокусом на задней камере? Ремонт занимает менее 1 часа.</li>
				<li class="breaking__item breaking__item--panel breaking__item--right">Ремонт задней панели: Изогнутая
					рамка, неработающие боковые кнопки или слишком много царапин будут удалены, когда мы снимем рамку
					задней панели.</li>
				<li class="breaking__item breaking__item--battery">Замена батареи: Если ваш Xiaomi включается, но не
					держит заряд слишком долго, вам может понадобиться новый аккумулятор. Ремонт занимает около 1 часа и
					требует предварительной записи.</li>
			</ul>
		</div>
	</section>

	<section class="quality">
		<div class="container">
			<h2 class="title">Качественный ремонт <strong>Xiaomi</strong> в Москве</h2>
			<ul class="quality__list">
				<li class="quality__item quality__item--one">С нашим сервисом вы можете получить качественный ремонт
					техники Xiaomi. Наша команда делает все, чтобы гарантировать вам отличные результаты ремонта продуктов
					бренда Xiaomi.</li>
				<li class="quality__item quality__item--two">Вы ищете лучший сервисный центр по ремонту мобильных
					телефонов Xiaomi в Москве? <br>Вы можете положиться на нас, чтобы удовлетворить ваши потребности
					наиболее эффективным способом и в короткие сроки. Как очень ответственный и надежный поставщик услуг,
					мы предлагаем современные ремонтные решения по самым конкурентоспособным ценам.</li>
				<li class="quality__item quality__item--three">Наши технические специалисты являются
					высококвалифицированными и опытными. Они знают, как выполнять ремонт всей техники Xiaomi наиболее
					эффективным способом. У них есть опыт, чтобы быстро обнаружить основную проблему и выполнить ремонтную
					задачу в течение одного часа с безупречной точностью.</li>
				<li class="quality__item quality__item--four">Мы предлагаем прозрачный подход для ответственной защиты
					ваших интересов. Предварительная цена позволяет избежать путаницы, и вы можете вернуться домой с
					полностью работающим устройством.</li>
			</ul>
		</div>
	</section>

	<section class="work-process">
		<div class="container">
			<h2 class="title">Процесс работы</h2>

			<?php
				$slides = carbon_get_post_meta(get_the_ID(), 'xiam_work_process');
				if ($slides):
			?>

			<div class="work-process__slider-wrapper">
				<div class="work-process__slider swiper-container">
					<ul class="work-process__list swiper-wrapper">

						<?php foreach ($slides as $v):
							$img_id = $v['xiam_work_slide_image'];
							$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
						?>

						<li class="work-process__item swiper-slide">
							<div class="work-process__image-wrapper">
								<a href="#">Записаться на ремонт</a>
								<img src="<?php echo get_template_directory_uri() . '/assets/images/main/work-process/slide_bg.png'?>" alt="<?=$alt;?>">
							</div>
							<div class="work-process__text-wrapper">
								<h3 class="work-process__headline"><?php echo $v['xiam_work_slide_title']; ?></h3>
								<div class="work-process__text">
									<?php echo $v['xiam_work_slide_text']; ?>
								</div>
							</div>
						</li>

						<?php endforeach; ?>

					</ul>
				</div>
				<div class="work-process__pagination"></div>
				<button class="work-process__nav-button work-process__nav-button--prev"></button>
				<button class="work-process__nav-button work-process__nav-button--next"></button>
			</div>

			<?php endif; ?>

		</div>
	</section>

<?php
get_footer();
