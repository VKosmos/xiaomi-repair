<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Xiaomi-repair
 */

get_header(null, ['page-remont']);
?>

	<section class="main-screen">
		<div class="main-screen__flex">
			<?php
				$img_url = wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(), 'xiar_devices_banner_image'), 'device-banner');
				$img_url_mobile = wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(), 'xiar_devices_banner_image_mobile'), 'device-banner');
			?>

			<img class="main-screen__bg" src="<?php echo $img_url; ?>">
			<img class="main-screen__bg-mobile" src="<?php echo $img_url_mobile; ?>">

			<div class="main-screen__text-wrap">
				<h1 class="main-screen__title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_devices_banner_title'); ?></h1>
				<p class="main-screen__text"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_devices_banner_text'); ?></p>

				<a class="main-screen__button _js-scroll" href="#" data-scroll=".price__title">Стоимость и сроки</a>

			</div>
		</div>
	</section>

	<section class="price">
		<div class="container">
			<h2 class="price__title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_devices_price-title');?></h2>
			<div class="price__table" data-device="<?php the_title();?>">
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__headline">
						<button class="price__button">Наименование услуги<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
						</div>
						<div class="price__headline">
						<button class="price__button">Стоимость<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
						</div>
						<div class="price__headline">
						<button class="price__button">Время работы<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
						</div>
						<div class="price__headline">
						<button class="price__button">Заказать<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
						</div>
					</div>
				</div>

				<?php
					$i = 0;

					$data = carbon_get_post_meta(get_the_ID(), 'xiar_repair_cat');
					if ($data):
						foreach ($data as $group):
							$i++;
						?>

						<div class="price__row-wrapper" <?php echo (9 == $i) ? 'data-hide-target="price"' : '';?>>
							<div class="price__row">
								<div class="price__head"><?php echo $group['xiar_repair_cat_name'];?></div>
							</div>
						</div>

						<?php

							foreach ($group['xiar_repair_services_list'] as $v):
								$i++;
							?>

								<div class="price__row-wrapper" <?php echo (9 == $i) ? 'data-hide-target="price"' : '';?>>
									<div class="price__row">
										<div class="price__name"><?=$v['xiar_repair_service'];?></div>
										<div class="price__price"><?=$v['xiar_repair_service_price'];?> руб.</div>
										<div class="price__time"><?=$v['xiar_repair_service_time'];?> мин.</div>
										<button class="price__order" data-vkpath="appointment" data-vkcontainer="1" data-service="<?=$v['xiar_repair_service'];?>">Заказать</button>
									</div>
								</div>

							<?php
							endforeach;

						endforeach;
					endif;
				?>


				<!-- <div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Экран</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена дисплея (экран)</div>
						<div class="price__price">755 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена тачскрина</div>
						<div class="price__price">755 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Плата</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена микросхемы</div>
						<div class="price__price">2100 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена платы</div>
						<div class="price__price">1290 руб.</div>
						<div class="price__time">45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена шлейфа платы</div>
						<div class="price__price">690 руб.</div>
						<div class="price__time">45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена платы GPS</div>
						<div class="price__price">690 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper" data-hide-target="price">
					<div class="price__row">
						<div class="price__head">Разъемы и порты</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить лоток Sim</div>
						<div class="price__price">800 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена разъема HDMI</div>
						<div class="price__price">590 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена гнезда питания</div>
						<div class="price__price">710 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Ремонт порта USB</div>
						<div class="price__price">590 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить разъем Micro, USB</div>
						<div class="price__price">795 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить лоток Flash</div>
						<div class="price__price">750 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Батарея</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Восстановление дорожек платы</div>
						<div class="price__price">400 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить аккумулятор</div>
						<div class="price__price">250 руб.</div>
						<div class="price__time">15 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Ремонт цепей питания платы</div>
						<div class="price__price">2800 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Аудио</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить аудио разъем</div>
						<div class="price__price">1100 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить динамик (слуховой)</div>
						<div class="price__price">350 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить микрофон</div>
						<div class="price__price">450 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">По</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Настроить программное обеспечение</div>
						<div class="price__price">500 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Прошить устройство с сохранением данных</div>
						<div class="price__price">3300 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Прошить устройство без сохранения данных</div>
						<div class="price__price">3550 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Корпус</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить заднюю крышку устройства</div>
						<div class="price__price">795 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Ремонт корпуса</div>
						<div class="price__price">890 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Полный разбор</div>
						<div class="price__price">590 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить кнопку включения/выключения</div>
						<div class="price__price">545 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">Другое</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить вибро элемент</div>
						<div class="price__price">450 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить камеру (внешнюю, внутреннюю)</div>
						<div class="price__price">450 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Заменить шлейф кнопок, дисплея</div>
						<div class="price__price">600 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена Wi-Fi модуля</div>
						<div class="price__price">490 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Замена контролера управления</div>
						<div class="price__price">1190 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Чистка пыли или влаги</div>
						<div class="price__price">790 руб.</div>
						<div class="price__time">от 45 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__head">НЕИСПРАВНОСТИ</div>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не работает bluetooth</div>
						<div class="price__price">от 710 руб.</div>
						<div class="price__time">от 20 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не видит сим карту</div>
						<div class="price__price">710 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не видит флеш карту</div>
						<div class="price__price">490 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не ловит сеть</div>
						<div class="price__price">710 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не работает Wi-Fi</div>
						<div class="price__price">440 руб.</div>
						<div class="price__time">30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не включается</div>
						<div class="price__price">от 440 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div>
				<div class="price__row-wrapper">
					<div class="price__row">
						<div class="price__name">Не заряжается</div>
						<div class="price__price">от 440 руб.</div>
						<div class="price__time">от 30 мин.</div>
						<button class="price__order">Заказать</button>
					</div>
				</div> -->
			</div>
			<button class="price__show-all" data-hide="price">Смотреть все категории</button>
		</div>
	</section>

<?php
get_footer();
