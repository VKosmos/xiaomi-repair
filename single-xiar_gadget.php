<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Xiaomi-repair
 */

get_header(null, ['page-remont']);
?>

	<section class="main-screen">
		<div class="main-screen__flex">
			<?php
				$title = get_the_title();

				$img_url = wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(), 'xiar_gadget_banner_image'), 'device-banner');
				$img_url_mobile = wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(), 'xiar_gadget_banner_image_mobile'), 'device-banner');
			?>

			<img class="main-screen__bg" src="<?php echo $img_url; ?>">
			<img class="main-screen__bg-mobile" src="<?php echo $img_url_mobile; ?>">

			<div class="main-screen__text-wrap">
				<h1 class="main-screen__title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_gadget_banner_title'); ?></h1>
				<p class="main-screen__text"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_gadget_banner_text'); ?></p>

				<a class="main-screen__button _js-scroll" href="#" data-scroll=".price__title">Стоимость и сроки</a>

			</div>
		</div>
	</section>

	<section class="price">
		<div class="container">
			<h2 class="price__title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_gadget_price-title');?></h2>

			<?php
				$data = carbon_get_post_meta(get_the_ID(), 'xiar_repair_cat');
				if (!empty($data)):
			?>

				<div class="price__table" data-device="<?php echo $title; ?>">
					<div class="price__row-wrapper">
						<div class="price__row">
							<div class="price__headline">
							<button class="price__button">Наименование услуги<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
							</div>
							<div class="price__headline">
							<button class="price__button">Стоимость<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
							</div>
							<div class="price__headline">
							<button class="price__button">Время работы<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
							</div>
							<div class="price__headline">
							<button class="price__button">Заказать<img src="<?php echo get_template_directory_uri() . '/assets/images/header/arrow.svg';?>" alt=""></button>
							</div>
						</div>
					</div>

				<?php
					$i = 0;
					foreach ($data as $group):
						$i++;
					?>

						<div class="price__row-wrapper" <?php echo (9 == $i) ? 'data-hide-target="price"' : '';?>>
							<div class="price__row">
								<div class="price__head"><?php echo $group['xiar_gadget_repair_cat_name'];?></div>
							</div>
						</div>

						<?php

							if ($group['xiar_gadget_repair_services_list']):

								foreach ($group['xiar_gadget_repair_services_list'] as $v):
									$i++;
								?>

									<div class="price__row-wrapper" <?php echo (9 == $i) ? 'data-hide-target="price"' : '';?>>
										<div class="price__row">
											<div class="price__name"><?php echo $v['xiar_gadget_repair_service'] . ' ' . $title;?></div>
											<div class="price__price"><?=$v['xiar_gadget_repair_service_price'];?> руб.</div>
											<div class="price__time"><?=$v['xiar_gadget_repair_service_time'];?> мин.</div>
											<button class="price__order" data-vkpath="appointment" data-vkcontainer="1" data-service="<?=$v['xiar_gadget_repair_service'];?>">Заказать</button>
										</div>
									</div>

								<?php
								endforeach;

							endif;

						endforeach;?>

				</div><?php /* .price-table */ ?>
				<button class="price__show-all" data-hide="price">Смотреть все категории</button>

			<?php endif;?>

		</div>
	</section>

<?php
get_footer();
