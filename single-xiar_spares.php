<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Xiaomi-repair
 */

get_header();
?>

	<section class="card-product">
		<div class="container">
			<div class="card-product__wrapper">

				<?php
					$imd_id = get_post_thumbnail_id( get_the_ID() );
					$img_url = get_the_post_thumbnail_url( get_the_ID(), 'spares-thumb');
					if ($img_id):
						$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
					endif;
				?>

					<div class="card-product__img-wrapper">
						<?php if ($img_url):?>
						<img class="card-product__img" src="<?php echo $img_url; ?>" alt="<?php echo $alt;?>">
						<?php endif; ?>
					</div>
					<div class="card-product__info-wrapper">
						<h2 class="card-product__title"><?php the_title(); ?></h2>

						<div class="card-product__text">
							<?php the_content(); ?>
						</div>

						<span class="card-product__price">Стоимость запчасти</span>
						<p class="card-product__buy"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_spares_price'); ?><span> руб.</span></p>
						<button class="card-product__button" data-vkpath="appointment" data-vkcontainer="1">Добавить в корзину</button>
					</div>


			</div>
		</div>
	</section>



<?php
get_footer();
