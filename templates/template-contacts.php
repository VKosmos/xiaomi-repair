<?php
/**
 * The Main page template
 *
 * Template Name: Контакты
 *
 * @package Xiaomi-repair
 */

get_header(null, ['page-contacts']);
?>

<section class="our-contacts">
	<div class="container">

		<div class="our-contacts__flex">
			<div class="our-contacts__column">
				<h2 class="our-contacts__headline"><strong>Контактная информация:</strong></h2>
				<p class="our-contacts__phone">
					<?php
						$phone = carbon_get_theme_option('xiam_phone');
						if ($phone):
							$phone_link = preg_replace("/[^+0-9]/", '', $phone);?>
							<a href="tel:<?php echo $phone_link;?>"><?php echo $phone;?></a>
						<?php endif; ?>
				</p>
				<p class="our-contacts__clarification">Прием заявок по телефону осуществляется круглосуточно</p>
				<p class="our-contacts__text">
					<strong>Адрес:</strong> <?php echo carbon_get_theme_option('xiam_address'); ?>
				</p>
				<p class="our-contacts__text"><strong>Часы работы сервисного центра:</strong><span>понедельник - пятница <?php echo carbon_get_theme_option('xiar_work_weekdays');?></span><span>суббота, воскресенье <?php echo carbon_get_theme_option('xiar_work_weekends');?></span></p>
			</div>
			<div class="our-contacts__map-wrapper">
				<div id="map" class="our-contacts__map"></div>
			</div>

			<span class="our-contacts__notice">Наши Контакты</span>
		</div>

		<div class="our-contacts__feedback">
			<h2 class="our-contacts__feedback-headline">Проконсультируйтесь по ремонту <strong>Xiaomi</strong> с мастером прямо сейчас</h2>
			<p class="our-contacts__feedback-text">Задайте ваш вопрос по ремонту через форму обратной связи и мастер свяжется с вами в течение 10-15 минут</p>

			<?php
				echo do_shortcode( '[contact-form-7 id="84" html_class="form-feedback__form form-feedback" title="Сообщение на странице Контакты"]');
			?>

			<span class="our-contacts__notice">Связаться</span>
		</div>

		<div class="our-contacts__notice-line"></div>
	</div>
</section>


<?php
get_footer();
