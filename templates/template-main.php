<?php
/**
 * The Main page template
 *
 * Template Name: Главная страница
 *
 * @package Xiaomi-repair
 */

get_header(null, ['page-main']);
?>

	<section class="main-screen">
		<div class="main-screen__flex">
			<div class="main-screen__text-wrap">
				<h1 class="main-screen__title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_banner_title'); ?></h1>
				<p class="main-screen__text"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_banner_text'); ?></p>
				<a class="main-screen__button" href="#" data-vkpath="appointment" data-vkcontainer="1">Записаться на ремонт</a>
			</div>
		</div>
	</section>

	<section class="reliable">
		<div class="container">
			<h2 class="title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_service_title'); ?></h2>
			<p class="reliable__headline"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_service_text'); ?></p>

			<?php
				$slides = carbon_get_post_meta(get_the_ID(), 'xiam_service_center');
				if ($slides):

			?>
			<div class="reliable__slider-wrapper">
				<div class="reliable__slider swiper-container">
					<ul class="reliable__list swiper-wrapper">

						<?php	foreach ($slides as $v):
							$img_id = $v['xiam_service_slide_image'];
							$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
						?>

						<li class="reliable__item swiper-slide">
							<div class="reliable__image-wrapper">
								<img src="<?php echo wp_get_attachment_image_url($img_id, 'service-slide'); ?>" alt="<?=$alt;?>">
							</div>
							<div class="reliable__text-wrapper">
								<h3 class="reliable__head"><?php echo $v['xiam_service_slide_title'];?></h3>
								<div class="reliable__text">
									<?php echo $v['xiam_service_slide_text'];?>
								</div>
							</div>
						</li>

						<?php endforeach;?>

					</ul>
				</div>
				<div class="reliable__pagination"></div>
				<button class="reliable__nav-button reliable__nav-button--prev"></button>
				<button class="reliable__nav-button reliable__nav-button--next"></button>
			</div>
			<?php endif;?>

			<div class="main-content-wrapper">
			<?php the_content(); ?>
			</div>

		</div>
	</section>

	<section class="fair">
		<div class="container">
			<h2 class="title title--left"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_fair_title'); ?></h2>
			<p class="fair__headline"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_fair_subtitle'); ?></p>
			<div class="fair__wrapper">
				<div class="fair__text-wrapper">
					<p><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_fair_text1'); ?></p>
				</div>
				<div class="fair__text-wrapper fair__text-wrapper--two">
					<p><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_fair_text2'); ?></p>
				</div>
				<div class="fair__text-wrapper fair__text-wrapper--three">
					<p><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_fair_text3'); ?></p>
				</div>
			</div>
		</div>
	</section>

	<section class="breaking">
		<div class="container">
			<h2 class="title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_title'); ?></h2>
			<ul class="breaking__list">
				<li class="breaking__item breaking__item--diagnostics"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text1'); ?></li>
				<li class="breaking__item breaking__item--soldering breaking__item--right"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text2'); ?></li>
				<li class="breaking__item breaking__item--screen"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text3'); ?></li>
				<li class="breaking__item breaking__item--charge breaking__item--right"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text4'); ?></li>
				<li class="breaking__item breaking__item--camera"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text5'); ?></li>
				<li class="breaking__item breaking__item--panel breaking__item--right"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text6'); ?></li>
				<li class="breaking__item breaking__item--battery"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_typical_text7'); ?></li>
			</ul>
		</div>
	</section>

	<section class="quality">
		<div class="container">
			<h2 class="title"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_quality_title'); ?></h2>
			<ul class="quality__list">
				<li class="quality__item quality__item--one"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_quality_text1'); ?></li>
				<li class="quality__item quality__item--two"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_quality_text2'); ?></li>
				<li class="quality__item quality__item--three"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_quality_text3'); ?></li>
				<li class="quality__item quality__item--four"><?php echo carbon_get_post_meta(get_the_ID(), 'xiar_main_quality_text4'); ?></li>
			</ul>
		</div>
	</section>

	<section class="work-process">
		<div class="container">
			<h2 class="title">Процесс работы</h2>

			<?php
				$slides = carbon_get_post_meta(get_the_ID(), 'xiam_work_process');
				if ($slides):
			?>

			<div class="work-process__slider-wrapper">
				<div class="work-process__slider swiper-container">
					<ul class="work-process__list swiper-wrapper">

						<?php foreach ($slides as $v):
							$img_id = $v['xiam_work_slide_image'];
							$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
						?>

						<li class="work-process__item swiper-slide">
							<div class="work-process__image-wrapper">
								<a href="#">Записаться на ремонт</a>
								<img src="<?php echo get_template_directory_uri() . '/assets/images/main/work-process/slide_bg.png'?>" alt="<?=$alt;?>">
							</div>
							<div class="work-process__text-wrapper">
								<h3 class="work-process__headline"><?php echo $v['xiam_work_slide_title']; ?></h3>
								<div class="work-process__text">
									<?php echo $v['xiam_work_slide_text']; ?>
								</div>
							</div>
						</li>

						<?php endforeach; ?>

					</ul>
				</div>
				<div class="work-process__pagination"></div>
				<button class="work-process__nav-button work-process__nav-button--prev"></button>
				<button class="work-process__nav-button work-process__nav-button--next"></button>
			</div>

			<?php endif; ?>

		</div>
	</section>

<?php
get_footer();
